#!/usr/bin/python

import os
import random
import time

pic_folder = "/home/shahzeb/Downloads/wallpapers"
list = (os.listdir(pic_folder))

while True:
    num = random.randint(0, len(list)-1)
    random.shuffle(list)
    change_background = "gsettings set org.gnome.desktop.background picture-uri \
                         file://%s/%s" % (pic_folder, list[num])

    os.system(change_background)
    time.sleep(25 * 1.618)
